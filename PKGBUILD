# Obarun      : 66 init/supervisor | LibreSSL   
# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/python2/
## Maintainer : Felix Yan <felixonmars@archlinux.org>
## Contributor: Stéphane Gaudreault <stephane@archlinux.org>
## Contributor: Allan McRae <allan@archlinux.org>
## Contributor: Jason Chu <jason@archlinux.org>
#--------------------------------------------------------------------------------------

pkgname=python2
pkgver=2.7.16
pkgrel=2
_pybasever=2.7
arch=('x86_64')
license=('PSF')
_website="http://www.python.org/"
pkgdesc="A high-level scripting language"
url="https://www.python.org/ftp/python"
source=("$url/$pkgver/Python-$pkgver.tar.xz"{,.asc})

conflicts=(
    'python<3')
depends=(
    'bzip2'
    'expat'
    'gdbm'
    'libffi'
    'libnsl'
    'libressl'
    'sqlite'
    'zlib')
makedepends=(
    'tk'
    'bluez-libs')
checkdepends=(
    'gdb'
    'file'
    'xorg-server-xvfb')
optdepends=(
    'tk: for IDLE'
    'python2-setuptools'
    'python2-pip')

#--------------------------------------------------------------------------------------
prepare() {
    cd "Python-$pkgver"

    ## Temporary workaround for FS#22322
    ## See http://bugs.python.org/issue10835 for upstream report
    sed -i "/progname =/s/python/python${_pybasever}/" Python/pythonrun.c

    ## Enable built-in SQLite module to load extensions (fix FS#22122)
    sed -i "/SQLITE_OMIT_LOAD_EXTENSION/d" setup.py

    ## FS#23997
    sed -i -e "s|^#.* /usr/local/bin/python|#!/usr/bin/python2|" Lib/cgi.py

    sed -i "s/python2.3/python2/g" Lib/distutils/tests/test_build_scripts.py \
     Lib/distutils/tests/test_install_scripts.py

    ## Ensure that we are using the system copy of various libraries (expat, zlib and libffi)
    ## rather than copies shipped in the tarball
    rm -r Modules/expat
    rm -r Modules/zlib
    rm -r Modules/_ctypes/{darwin,libffi}*

    ## clean up #!s
    find . -name '*.py' | \
    xargs sed -i "s|#[ ]*![ ]*/usr/bin/env python$|#!/usr/bin/env python2|"

    ## Workaround asdl_c.py/makeopcodetargets.py errors after we touched the shebangs
    touch Include/Python-ast.h Python/Python-ast.c Python/opcode_targets.h

    ## FS#48761
    ## http://bugs.python.org/issue25750
    #patch -Np1 -i ../descr_ref.patch

    ## Don't fail if pygments is not available
    #patch -Np1 -i ../Dont-fail-if-pygments-is-not-available.patch
}

build() {
    cd "Python-$pkgver"

    export OPT="${CFLAGS}"

    ./configure \
        --prefix=/usr \
        --enable-ipv6 \
        --enable-optimizations \
        --enable-shared \
        --enable-unicode=ucs4 \
        --with-dbmliborder=gdbm:ndbm \
        --with-lto \
        --with-system-expat \
        --with-system-ffi \
        --with-threads \
        --without-ensurepip
	make
}

check() {

    ## Since 2.7.14 with latest xvfb
    ## test_idle, test_tk, test_ttk_guionly: segfaults
    ## Since 2.7.15: test_ctypes

    cd "Python-$pkgver"

    LD_LIBRARY_PATH="${srcdir}/Python-$pkgver":${LD_LIBRARY_PATH} \
    xvfb-run "${srcdir}/Python-$pkgver/python" -m test.regrtest -v -uall -x test_idle test_tk test_ttk_guionly test_ctypes
}

package() {
    cd "Python-$pkgver"

    ## Hack to avoid building again
    sed -i 's/^all:.*$/all: build_all/' Makefile

    make DESTDIR="${pkgdir}" altinstall maninstall

    rm "${pkgdir}"/usr/share/man/man1/python.1

    ln -sf python${_pybasever}        "${pkgdir}"/usr/bin/python2
    ln -sf python${_pybasever}-config "${pkgdir}"/usr/bin/python2-config
    ln -sf python${_pybasever}.1      "${pkgdir}"/usr/share/man/man1/python2.1

    ## FS#33954
    ln -sf python-${_pybasever}.pc    "${pkgdir}"/usr/lib/pkgconfig/python2.pc

    ln -sf ../../libpython${_pybasever}.so "${pkgdir}"/usr/lib/python${_pybasever}/config/libpython${_pybasever}.so
    mv "${pkgdir}"/usr/bin/smtpd.py "${pkgdir}"/usr/lib/python${_pybasever}/

    ## some useful "stuff"
    install -dm755 "${pkgdir}"/usr/lib/python${_pybasever}/Tools/{i18n,scripts}
    install -m755 Tools/i18n/{msgfmt,pygettext}.py "${pkgdir}"/usr/lib/python${_pybasever}/Tools/i18n/
    install -m755 Tools/scripts/{README,*py} "${pkgdir}"/usr/lib/python${_pybasever}/Tools/scripts/

    ## fix conflicts with python
    mv "${pkgdir}"/usr/bin/idle{,2}
    mv "${pkgdir}"/usr/bin/pydoc{,2}
    mv "${pkgdir}"/usr/bin/2to3{,-2.7}

    ## clean-up reference to build directory
    sed -i "s#${srcdir}/Python-$pkgver:##" "${pkgdir}"/usr/lib/python${_pybasever}/config/Makefile

    ## license
    install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/$pkgname/LICENSE
}

#--------------------------------------------------------------------------------------
validpgpkeys=('C01E1CAD5EA2C4F0B8E3571504C367C218ADD4FF' # Benjamin Peterson
)
sha512sums=('16e814e8dcffc707b595ca2919bd2fa3db0d15794c63d977364652c4a5b92e90e72b8c9e1cc83b5020398bd90a1b397dbdd7cb931c49f1aa4af6ef95414b43e0'
            'SKIP')
